import 'package:custom_widgets/commands/commands.dart';
import 'package:custom_widgets/components/container_shape_component.dart';
import 'package:custom_widgets/controls.dart';
import 'package:custom_widgets/shape_container.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ContainerModel containerModel = ContainerModel.initial();
  CommandHistory commandHistory = CommandHistory();

  void rebuild(v) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Shape Component"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ShapeContainer(shape: containerModel),
            const SizedBox(
              height: 50,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: containerModel.controls.length,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                          "Change ${containerModel.controls[index].propertyName} :"),
                      const SizedBox(
                        width: 10,
                      ),
                      WidgetControlls.controll(containerModel.controls[index],
                          rebuild, containerModel),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
