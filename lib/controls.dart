import 'package:custom_widgets/commands/command.dart';
import 'package:custom_widgets/commands/commands.dart';
import 'package:cyclop/cyclop.dart';
import 'package:flutter/material.dart';

class WidgetControlls {
  static Widget controll(
      Control control, Function(dynamic v) updateUi, dynamic component) {
    Widget controlWidget;

    control.onChange = updateUi;

    switch (control.controlType) {
      case ControlType.colorPicker:
        controlWidget = ColorPickerControl(
          color: control.initialValue,
          shape: BoxShape.rectangle, // default : circle
          size: 32,
          onChange: (color, c) => control.onControlChanged(
            color,
            ChangeColorCommand(
              component,
              color,
            ),
          ),
          command: control.command,
        ).drawColorPickerControl();
        break;
      case ControlType.sliderHight:
        controlWidget = SliderControl(
          max: 300,
          min: 10,
          value: control.initialValue,
          command: control.command,
          onChange: (val, c) => control.onControlChanged(
              val, ChangeHeightCommand(component, val)),
        ).drawSliderControl();

        break;
      case ControlType.sliderWidth:
        controlWidget = SliderControl(
          max: 300,
          min: 5,
          value: control.initialValue,
          command: control.command,
          onChange: (val, c) =>
              control.onControlChanged(val, ChangeWidthCommand(component, val)),
        ).drawSliderControl();
        break;
      default:
        controlWidget = Container();
    }
    return controlWidget;
  }
}


enum ControlType {
  sliderHight,
  sliderWidth,
  colorPicker,
  switchControl,
  select,
  check,
}

class Control<T> {
  ControlType controlType;
  Command command;
  String propertyName;
  dynamic initialValue;
  Function(T val) onChange;

  Control({
    required this.propertyName,
    required this.controlType,
    required this.command,
    required this.initialValue,
    required this.onChange,
  });

  void onControlChanged(T val, Command updatedCommand) {
    print("Pressed Value -> $val");
    command = updatedCommand;
    onChange(val);
    command.execute();
  }
}

class ColorPickerControl {
  Color color;
  double size;
  Command command;
  BoxShape shape;
  Function(Color val, Command updatedCommand) onChange;

  ColorPickerControl({
    required this.color,
    required this.size,
    required this.shape,
    required this.command,
    required this.onChange,
  });

  ColorButton drawColorPickerControl() {
    return ColorButton(
        color: color,
        size: size,
        boxShape: shape,
        onColorChanged: (color) {
          onChange(
            color,
            command,
          );
        });
  }
}

class SliderControl {
  double max;
  Command command;
  double min;
  double value;
  Function(double val, Command updatedCommand) onChange;

  SliderControl(
      {required this.max,
      required this.min,
      required this.value,
      required this.command,
      required this.onChange});

  Slider drawSliderControl() {
    return Slider(
      value: value,
      onChanged: (double val) {
        onChange(val, command);
      },
      min: min,
      max: max,
    );
  }
}
