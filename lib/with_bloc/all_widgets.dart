import 'package:flutter/material.dart';

class AllWidgetsScreen extends StatelessWidget {
  const AllWidgetsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("All Widgets"),
      ),
      body: Column(
        children: [
          TextButton(
            onPressed: () {},
            child: const Text("Container Widget"),
          ),
        ],
      ),
    );
  }
}
