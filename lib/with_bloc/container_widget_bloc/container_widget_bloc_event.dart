part of 'container_widget_bloc_bloc.dart';

sealed class ContainerWidgetBlocEvent {}

class ChangeColorEvent extends ContainerWidgetBlocEvent {
  final Color color;
  ChangeColorEvent({required this.color});
}

class ChangeWidthEvent extends ContainerWidgetBlocEvent {
  final double width;
  ChangeWidthEvent({required this.width});
}

class ChangeHeightEvent extends ContainerWidgetBlocEvent {
  final double height;
  ChangeHeightEvent({required this.height});
}
