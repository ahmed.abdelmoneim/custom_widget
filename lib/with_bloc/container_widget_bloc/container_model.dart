import 'package:custom_widgets/with_bloc/container_widget_bloc/container_widget_bloc_bloc.dart';
import 'package:flutter/material.dart';

enum Shape { recangle, circle }

// Shape or Receiver
class ContainerModelBloc {
  late Color color;
  late double height;
  late double width;
  late List<Control> controls;

  ContainerModelBloc.initial() {
    color = Colors.red;
    height = 100;
    width = 100;
    controls = [
      Control<Color>(
        propertyName: "Color",
        controlType: ControlType.colorPicker,
        event: ChangeColorEvent(color: color),
        initialValue: color,
        onChange: (Color c) {
          color = c;
        },
      ),
      Control<double>(
        propertyName: "Height",
        event: ChangeHeightEvent(height: height),
        controlType: ControlType.sliderHight,
        initialValue: height,
        onChange: (double h) {
          height = h;
        },
      ),
      Control<double>(
        propertyName: "Width",
        event: ChangeWidthEvent(width: width),
        controlType: ControlType.sliderWidth,
        initialValue: width,
        onChange: (double w) {
          width = w;
        },
      ),
    ];
  }
}

enum ControlType {
  sliderHight,
  sliderWidth,
  colorPicker,
  switchControl,
  select,
  check,
}

class Control<T> {
  ControlType controlType;
  Object event;
  String propertyName;
  dynamic initialValue;
  Function(T val) onChange;

  Control({
    required this.propertyName,
    required this.controlType,
    required this.event,
    required this.initialValue,
    required this.onChange,
  });

  void onControlChanged(T val, Object xEvent) {
    event = xEvent;
    onChange(val);
  }
}
