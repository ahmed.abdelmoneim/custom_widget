import 'dart:ui';

import 'package:flutter_bloc/flutter_bloc.dart';

part 'container_widget_bloc_event.dart';
part 'container_widget_bloc_state.dart';

class ContainerWidgetBlocBloc
    extends Bloc<ContainerWidgetBlocEvent, ContainerWidgetBlocState> {
  ContainerWidgetBlocBloc() : super(Initial()) {
    on<ContainerWidgetBlocEvent>((event, emit) {});
  }
}
