part of 'container_widget_bloc_bloc.dart';


sealed class ContainerWidgetBlocState {}

final class Initial extends ContainerWidgetBlocState {}
final class Loading extends ContainerWidgetBlocState {}
final class Success extends ContainerWidgetBlocState {}
final class Failed extends ContainerWidgetBlocState {}
