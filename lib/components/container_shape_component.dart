import 'package:custom_widgets/commands/commands.dart';
import 'package:custom_widgets/controls.dart';
import 'package:flutter/material.dart';

enum Shape { recangle, circle }

// Shape or Receiver
class ContainerModel {
  late Color color;
  late double height;
  late double width;
  late List<Control> controls;

  ContainerModel.initial() {
    color = Colors.red;
    height = 100;
    width = 100;
    controls = [
      Control<Color>(
        propertyName: "Color",
        controlType: ControlType.colorPicker,
        command: ChangeColorCommand(this, color),
        initialValue: color,
        onChange: (Color c) {
          color = c;
        },
      ),
      Control<double>(
        propertyName: "Height",
        command: ChangeHeightCommand(this, height),
        controlType: ControlType.sliderHight,
        initialValue: height,
        onChange: (double h) {
          height = h;
        },
      ),
      Control<double>(
        propertyName: "Width",
        command: ChangeWidthCommand(this, width),
        controlType: ControlType.sliderWidth,
        initialValue: width,
        onChange: (double w) {
          width = w;
        },
      ),
    ];
  }
}
