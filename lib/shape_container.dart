import 'package:custom_widgets/components/container_shape_component.dart';
import 'package:flutter/material.dart';

class ShapeContainer extends StatelessWidget {
  final ContainerModel shape;

  const ShapeContainer({
    super.key,
    required this.shape,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 160.0,
      child: Center(
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 500),
          height: shape.height,
          width: shape.width,
          decoration: BoxDecoration(
            color: shape.color,
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }
}
