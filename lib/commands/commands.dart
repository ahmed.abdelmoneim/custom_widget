
import 'dart:collection';

import 'package:custom_widgets/commands/command.dart';
import 'package:custom_widgets/components/container_shape_component.dart';
import 'package:custom_widgets/controls.dart';
import 'package:flutter/material.dart';

class ChangeColorCommand implements Command {
  ContainerModel containerModel;
  late Color previousColor;
  Color newColor;

  ChangeColorCommand(this.containerModel, this.newColor) {
    previousColor = containerModel.color;
  }

  @override
  void execute() {
    containerModel.color = newColor;
    int index = containerModel.controls.indexWhere(
        (control) => control.controlType == ControlType.colorPicker);
    containerModel.controls[index].initialValue = newColor;
    print("New Color -> $newColor");
  }

  @override
  String getTitle() {
    return 'Change Color';
  }

  @override
  void undo() {
    containerModel.color = previousColor;
  }
}

class ChangeHeightCommand implements Command {
  ContainerModel shapeComponent;
  late double previousHeight;
  double newHeight;

  ChangeHeightCommand(this.shapeComponent, this.newHeight) {
    previousHeight = shapeComponent.height;
  }

  @override
  void execute() {
    shapeComponent.height = newHeight;
    int index = shapeComponent.controls.indexWhere(
        (control) => control.controlType == ControlType.sliderHight);
    shapeComponent.controls[index].initialValue = newHeight;

    print("New Height -> $newHeight");
  }

  @override
  String getTitle() {
    return 'Change Height';
  }

  @override
  void undo() {
    shapeComponent.height = previousHeight;
  }
}

class ChangeWidthCommand implements Command {
  ContainerModel shapeComponent;
  late double previousWidth;
  double newWidth;

  ChangeWidthCommand(this.shapeComponent, this.newWidth) {
    previousWidth = shapeComponent.width;
  }

  @override
  void execute() {
    shapeComponent.width = newWidth;
    int index = shapeComponent.controls.indexWhere(
        (control) => control.controlType == ControlType.sliderWidth);
    shapeComponent.controls[index].initialValue = newWidth;
  }

  @override
  String getTitle() {
    return 'Change Height';
  }

  @override
  void undo() {
    shapeComponent.width = previousWidth;
  }
}

class CommandHistory {
  final ListQueue<Command> _commandList = ListQueue<Command>();
  bool get isEmpty => _commandList.isEmpty;
  List<String> get commandHistoryList =>
      _commandList.map((c) => c.getTitle()).toList();

  void add(Command command) {
    _commandList.add(command);
  }

  void undo() {
    if (_commandList.isNotEmpty) {
      var command = _commandList.removeLast();
      command.undo();
    }
  }
}