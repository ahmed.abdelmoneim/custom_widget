import 'dart:ui';

class CyTextComponent {
  String value;
  List<Property>? properties;
  Action? action;

  CyTextComponent({
    required this.value,
    required this.properties,
    required this.action,
  });
}

class Property {}

class TextStyleProperty extends Property {
  Color color;
  Color bgColor;
  double fontSize;
  FontWeight fontWeight;
  FontStyle fontStyle;
  double letterSpacing;
  double wordSpacing;

  Color get getTxtColor {
    return color;
  }

  set setTxtColor(Color newColor) {
    color = newColor;
  }

  Color get getTxtBGColor {
    return color;
  }

  set setTxtBGColor(Color newBGColor) {
    color = newBGColor;
  }

  double get getFontSize {
    return fontSize;
  }

  set setFontSize(double size) {
    fontSize = size;
  }

  FontWeight get getFontWeight {
    return fontWeight;
  }

  set setFontWeight(FontWeight weight) {
    fontWeight = weight;
  }

  TextStyleProperty({
    required this.color,
    required this.bgColor,
    required this.fontSize,
    required this.fontStyle,
    required this.fontWeight,
    required this.letterSpacing,
    required this.wordSpacing,
  });
}

class Action {}